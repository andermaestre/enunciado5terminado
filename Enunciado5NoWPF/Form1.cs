﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClasesSocket;

namespace Enunciado5NoWPF
{
    public partial class Form1 : Form
    {
        Servidor s;
        public delegate void CuentaLista(ListBox lb);
        public delegate void ModaLista(ListBox lb);
        public delegate void NuevoLista(ListBox lb, String nom);
        public Form1()
        {
            InitializeComponent();
        }

        private void btnArrancar_Click(object sender, EventArgs e)
        {
            s = new Servidor(int.Parse(txtSocket.Text), txtHost.Text, int.Parse(txtMax.Text));
            s.Start();
            s.OnMaximoalcanzado += () =>
            {
                MessageBox.Show("Demasiada gente!!");
            };
            s.OnDatosRecibidos += S_OnDatosRecibidos;
            btnArrancar.Enabled = false;
        }

        private void S_OnDatosRecibidos(string datos, int pos)
        {
            if (datos.CompareTo("cuantos?") == 0)
            {
                CuentaLista f = CuentaTodos;
                lb.BeginInvoke(f, new object[] { lb });
            }else if (datos.CompareTo("max?") == 0)
            {
                ModaLista f = ModaFun;
                lb.BeginInvoke(f, new object[] { lb });
            }
            else
            {
                NuevoLista f = NuevoPardo;
                lb.BeginInvoke(f, new object[] { lb ,datos});
            }
        }

        private void NuevoPardo(ListBox lb, string nom)
        {
            lb.Items.Add(nom);
            s.EnviarDatos(nom);
        }

        private void ModaFun(ListBox lb)
        {
            string res="Nadie";
            int max=0;
            for ( int i = 0; i < lb.Items.Count; i++)
            {
                int cont = 0;
                for(int j = i + 1; j < lb.Items.Count; j++)
                {
                    if (lb.Items[i].ToString().CompareTo(lb.Items[j].ToString())==0)
                    {
                        cont++;
                    }
                }
                if (cont > max)
                {
                    res = lb.Items[i].ToString();
                    max = cont;
                }
            }
            s.EnviarDatos("1;"+res);
        }

        private void CuentaTodos(ListBox lb)
        {
            s.EnviarDatos("0;"+lb.Items.Count.ToString());
        }
    }
}
