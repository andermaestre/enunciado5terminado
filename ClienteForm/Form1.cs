﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClasesSocket;

namespace ClienteForm
{
    public partial class Form1 : Form
    {
        Cliente c;
        public Form1()
        {
            InitializeComponent();
        }

        private void btnConectar_Click(object sender, EventArgs e)
        {
            c = new Cliente(txtHost.Text, int.Parse(txtSocket.Text));
            c.Conectar();

            c.OnConectionRefused += () =>
            {
                MessageBox.Show("Esta lleno Campeón!!");
            };
            c.OnDatosRecibidos += C_OnDatosRecibidos;
        }

        private void C_OnDatosRecibidos(string datos)
        {
            string []res = datos.Split(new char[] { ';' });
            if (res[0].CompareTo("0") == 0)
            {
                MessageBox.Show(string.Format("Hay un total de {0} campos", res[1]));
            }
            else if(res[0].CompareTo("1") == 0)
            {
                MessageBox.Show(string.Format("{0} esta de moda!!", res[1]));
            }
            else
            {
                MessageBox.Show(string.Format("Se ha anhadido a la lista a : {0}", datos));
            }
        }

        private void txtEnviar_Click(object sender, EventArgs e)
        {
            c.EnviarDatos(txtPalaber.Text);
        }
    }
}
